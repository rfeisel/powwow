const input = [
    { "name": "item1", "isExpired": false},
    { "name": "item2", "isExpired": true},
    { "name": "item3", "isExpired": true},
    { "name": "item4", "isExpired": false},
    { "name": "item5", "isExpired": false}
  ];

let isExpired = [];
let output = [];

input.forEach((item) => {
    if(item.isExpired) {
        isExpired.push(item);
    } else {
        output.push(item);
    }
});

output = output.concat(isExpired);

console.log(output);