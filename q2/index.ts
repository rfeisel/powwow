#!/usr/bin/env node

class TreeNode {
    public children: Array<TreeNode>;
    constructor(public data: string){
        this.data = data;
        this.children = [];
    }
    static addNode(path: string, treenode: TreeNode){
        const paths = path.split('.');
        const child = new TreeNode(paths.shift());
        path = paths.join('.');

        if(!path) {
            treenode.children.push(child);
        } else {
            // TODO : Optimize this lookup for Node env
            const found = treenode.children.filter((value) => {
                return value.data === child.data;
            });

            if(found[0]){
                TreeNode.addNode(path, found[0]);
            } else {
                treenode.children.push(child);
                TreeNode.addNode(path, child);
            }
        }
    }
    static showNode(node: TreeNode, indent: number = 0){
        let output: string = '';

        for(let i = 0; i<indent; i++) {
            output += '-';
        }
        output += node.data;
        console.log(output);

        indent += 1;
        node.children.forEach(child => {
            TreeNode.showNode(child, indent);
        })
    }
}

class Tree {
    // Here we import the File System module of node
    private fs = require('fs');
    public root: TreeNode;
    constructor(){
        this.root = new TreeNode('root')
    }
    importData(file: string) {
        return new Promise((resolve, reject) => {
            this.fs.readFile(file, (err, data) => {
                data = JSON.parse(data);
                if (err) reject(err);
                else resolve(data);
            });
          });
    }
    static showTree(tree: Tree){
        TreeNode.showNode(tree.root);
    }
}

const tree = new Tree();
tree.importData('q2data.json')
.then(data => {
    for(var index in data){
        TreeNode.addNode(data[index], tree.root);
    }
    Tree.showTree(tree);
}, err => {
    console.log("some error happened!");
});
