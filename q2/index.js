#!/usr/bin/env node
var TreeNode = /** @class */ (function () {
    function TreeNode(data) {
        this.data = data;
        this.data = data;
        this.children = [];
    }
    TreeNode.addNode = function (path, treenode) {
        var paths = path.split('.');
        var child = new TreeNode(paths.shift());
        path = paths.join('.');
        if (!path) {
            treenode.children.push(child);
        }
        else {
            // TODO : Optimize this lookup for Node env
            var found = treenode.children.filter(function (value) {
                return value.data === child.data;
            });
            if (found[0]) {
                TreeNode.addNode(path, found[0]);
            }
            else {
                treenode.children.push(child);
                TreeNode.addNode(path, child);
            }
        }
    };
    TreeNode.showNode = function (node, indent) {
        if (indent === void 0) { indent = 0; }
        var output = '';
        for (var i = 0; i < indent; i++) {
            output += '-';
        }
        output += node.data;
        console.log(output);
        indent += 1;
        node.children.forEach(function (child) {
            TreeNode.showNode(child, indent);
        });
    };
    return TreeNode;
}());
var Tree = /** @class */ (function () {
    function Tree() {
        // Here we import the File System module of node
        this.fs = require('fs');
        this.root = new TreeNode('root');
    }
    Tree.prototype.importData = function (file) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.fs.readFile(file, function (err, data) {
                data = JSON.parse(data);
                if (err)
                    reject(err);
                else
                    resolve(data);
            });
        });
    };
    Tree.showTree = function (tree) {
        TreeNode.showNode(tree.root);
    };
    return Tree;
}());
var tree = new Tree();
tree.importData('q2data.json')
    .then(function (data) {
    for (var index in data) {
        TreeNode.addNode(data[index], tree.root);
    }
    Tree.showTree(tree);
}, function (err) {
    console.log("some error happened!");
});
//# sourceMappingURL=index.js.map