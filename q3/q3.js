const items = [
    "animals.dogs.poodle",
   "animals.cats.tabby",
   "animals.cats.siamese",
   "animals.dogs.labrador",
   "animals.dogs.hound",
   "plants.trees",
   "animals.birds.parrot.grey"
  ]

const term = process.argv[2];
let paths;
items.forEach(value => {
    paths = value.split('.');
    if(paths.includes(term)) console.log(value);
})