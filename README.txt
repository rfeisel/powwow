Please complete all 4 questions, and send back responses as soon as possible.

Although the sample data sets are small, the best answers are those that optimize for run
time and memory and run efficiently with even large data sets.

Please note that Question #3 builds on Question #2.

